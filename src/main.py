import copy
import os
import random
import time
import webbrowser

import pygame

import config


def get_time():
    '''获取当前时间(毫秒)'''
    return time.time()*1000

# 地图类,游戏地图


class Map:
    def __init__(self, row_num: int, col_num: int, block_size: int, block_surfaces: dict):
        self.width = col_num*block_size  # 地图宽度
        self.height = row_num*block_size  # 地图高度
        self.block_size = block_size  # 地图格子边长
        self.row_num = row_num  # 行数
        self.col_num = col_num  # 列数
        self.block_surfaces = block_surfaces  # 地图贴图
        self.x_offset = 0  # x轴偏移量,向右为正
        self.y_offset = 0  # y轴偏移量,向下为正
        self.map_ = []  # 地图数组
        self.create_food_interval = 3000  # 创建食物间隔
        self.last_create_food_time = 0  # 上次创建食物时间
        self.create_animal_interval = 6000  # 创建动物间隔
        self.last_create_animal_time = 0  # 上次创建动物时间
        self.padding = 200  # 地图滚动内边距
        self.speed = 1  # 地图滚动速度
        # 根据配置创建地图
        for i in range(self.row_num):
            line = []
            for j in range(self.col_num):
                if config.MAP[i][j]:
                    line.append(config.BLOCK_TYPE['wall'])
                else:
                    line.append(config.BLOCK_TYPE['null'])
            self.map_.append(line)

    def draw(self, surface: pygame.Surface):
        '''绘制地图,surface:绘制对象'''

        for i, row in enumerate(self.map_):
            for j, cell in enumerate(row):
                # 先绘制背景草地
                surface.blit(self.block_surfaces[config.BLOCK_TYPE['null']], (
                    self.x_offset+j*self.block_size, self.y_offset+i*self.block_size))
                # 如果草地上有实体,绘制
                if cell != config.BLOCK_TYPE["null"]:
                    surface.blit(
                        self.block_surfaces[cell], (self.x_offset+j*self.block_size, self.y_offset+i*self.block_size))

    def create_food(self):
        '''创建食物'''

        # 控制执行时间间隔
        if get_time()-self.last_create_food_time < self.create_food_interval:
            return
        self.last_create_food_time = get_time()

        # 随机生成坐标
        x = random.randint(0, self.col_num-1)
        y = random.randint(0, self.row_num-1)
        while True:
            # 如果目标位置不为空,重新生成
            if self.map_[x][y] != config.BLOCK_TYPE['null']:
                x = random.randint(0, self.col_num-1)
                y = random.randint(0, self.row_num-1)
            else:
                break
        # 创建食物
        self.map_[x][y] = config.BLOCK_TYPE['food']

    def create_animal(self):
        '''创建动物'''

        # 控制执行时间间隔
        if get_time()-self.last_create_animal_time < self.create_animal_interval:
            return
        self.last_create_animal_time = get_time()

        # 随机生成坐标
        x = random.randint(0, self.col_num-1)
        y = random.randint(0, self.row_num-1)
        while True:
            # 如果坐标处不为空,重新生成
            if self.map_[x][y] != config.BLOCK_TYPE['null']:
                x = random.randint(0, self.col_num-1)
                y = random.randint(0, self.row_num-1)
            else:
                break

        # 创建并加入动物
        animal = Animal(y, x, self)
        Game.add_animal(animal)

    def check_is_out(self, row: int, col: int):
        '''
        检查指定坐标是否超出地图范围
        args:
            row:行
            col:列
        '''
        return row >= self.col_num or col >= self.row_num or row < 0 or col < 0

    def update(self, x: int, y: int):
        '''
        根据蛇的坐标进行屏幕滚动
        '''

        # 左边界
        if x+self.x_offset < self.padding:
            if self.x_offset+self.speed < 0:
                self.x_offset += self.speed
            else:
                self.x_offset = 0

        # 右边界
        if x+self.x_offset > Game.width-self.padding:
            if self.x_offset+self.width > Game.width:
                self.x_offset -= self.speed
            else:
                self.x_offset = Game.width-self.width

        # 上边界
        if y+self.y_offset < self.padding:
            if self.y_offset+self.speed < 0:
                self.y_offset += self.speed
            else:
                self.y_offset = 0

        # 下边界
        if y+self.y_offset > Game.height-self.padding:
            if self.y_offset+self.height > Game.height:
                self.y_offset -= self.speed
            else:
                self.y_offset = Game.height-self.height


class Audio:
    '''
    声音播放类
    init():加载声音
    play_music(name:str)播放背景音乐
    play_sound(name:str)播放音效
    '''
    @classmethod
    def init(cls):
        '''初始化声音'''
        cls.sounds = config.init_sounds()
        cls.music = config.init_music()

    @classmethod
    def play_music(cls, name: str):
        '''
        播放背景音乐
        args:
            name:str 音乐名称
        '''
        pygame.mixer.music.load(cls.music[name])
        pygame.mixer.music.play()

    @classmethod
    def play_sound(cls, name):
        '''
        播放背景音效
        args:
            name:str 音效名称
        '''
        cls.sounds[name].play()


class Block:
    '''
    蛇的砖块类
    '''

    def __init__(self, row: int, col: int, map_: Map):
        '''
        初始化
        args:
            row:行位置
            col:列位置
            map_:游戏地图
        '''
        self.row = row  # 行位置
        self.col = col  # 列位置
        self.map_ = map_  # 游戏地图
        self.is_head = False  # 是否为贪吃蛇头部

    def update(self):
        '''更新地图数组'''
        self.map_.map_[self.row][self.col] = \
            config.BLOCK_TYPE["snake_head"] if self.is_head else config.BLOCK_TYPE["snake_body"]

    def reset(self):
        '''将自身位置的地图数组设为空'''
        self.map_.map_[self.row][self.col] = config.BLOCK_TYPE["null"]


class Snake:
    def __init__(self, block_position: list, map_: Map):
        '''
        初始化
        block_position:砖块位置,[(row,col),(row,col)...]
        '''
        self.blocks = []  # 组成蛇身的砖块
        self.direction = 'right'  # 蛇的前进方向

        # 根据砖块位置创建砖块
        for p in block_position:
            self.blocks.append(Block(p[0], p[1], map_))
        self.blocks[0].is_head = True

        self.map_ = map_  # 游戏地图
        self.last_update_time = 0  # 上次更新时间
        self.interval = 100  # 更新间隔(毫秒)
        self.no_meat = 0  # 没有吃肉的时间(更新数(100毫秒))
        self.no_meat_limit = 1400  # 没有吃肉的时间上限(超过就死)

    def update(self):
        '''
        贪吃蛇逻辑处理
        '''

        # 控制执行间隔
        if get_time()-self.last_update_time < self.interval:
            return
        self.last_update_time = get_time()

        # 判断是否加速
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LCTRL]:
            self.interval = 100
        else:
            self.interval = 200

        # 创建新的头坐标
        last_block = Block(
            self.blocks[0].row+config.direction[self.direction][1],
            self.blocks[0].col+config.direction[self.direction][0],
            self.map_
        )

        # 又有一次没吃肉
        self.no_meat += 1

        # 判断死亡
        if self.is_die():
            Game.gameOver()
            return

        # 如果顶到头了,直接返回(游戏规则设定撞边不死)
        if self.map_.check_is_out(last_block.row, last_block.col):
            return

        # 新砖块位置
        new_pos_block = self.map_.map_[last_block.row][last_block.col]

        # 如果前方是动物,死亡
        if new_pos_block in config.ANIMAL_TYPE:
            Game.gameOver()
            return

        # 如果前方是障碍,不向前走
        if new_pos_block in config.OBSTACLE_TYPE:
            return

        can_grow = False  # 是否可以生长
        if new_pos_block in config.FOOD_TYPE:  # 如果前方是食物
            self.map_.map_[
                last_block.row][last_block.col] = config.BLOCK_TYPE['null']
            can_grow = True

            # 计算生长位置
            grow_row = self.blocks[-1].row
            grow_col = self.blocks[-1].col

            # 播放吃东西音效
            Audio.play_sound("eat")

            # 如果是肉,重置没吃肉时间
            if new_pos_block == config.BLOCK_TYPE['meat']:
                self.no_meat = 0

        # 移动一格
        for block in self.blocks:
            block.reset()
            block_bak = copy.copy(block)
            block.row = last_block.row
            block.col = last_block.col
            last_block = block_bak

        # 如果能,生长
        if can_grow:
            self.blocks.append(Block(grow_row, grow_col, self.map_))

        # 砖块更新
        for block in self.blocks:
            block.update()

    def turn(self, direction: str):
        '''
        转向
        args:
            direction:方向,(up/down/left/right)
        '''

        # 如果是当前方向或是相反方向,返回
        if direction == self.direction or direction == config.direction[self.direction][2]:
            return

        # 转向
        self.direction = direction

    def is_die(self):
        '''
        判断是否死亡
        return:
            是否死亡:bool
        '''

        # 太久没吃肉会死
        if self.no_meat > self.no_meat_limit:
            return True

        # 如果蛇头四周全部都有障碍(蛇走不了了),死亡
        head = self.blocks[0]
        for direction in config.direction:
            # 跳过"停止"
            if direction == 'stop':
                continue
            dx, dy, _ = config.direction[direction]
            row, col = head.row+dy, head.col+dx
            if not self.map_.check_is_out(row, col) and self.map_.map_[row][col] not in config.OBSTACLE_TYPE:
                return False
        return True

    def draw_text(self, surface: pygame.Surface):
        '''
        绘制文字(当前缺肉程度)
        args:
            surface:要绘制的surface
        '''
        english_font = pygame.font.Font(os.path.join(
            os.path.dirname(__file__), './font/mouse.otf'), 24)
        chinese_font = pygame.font.Font(os.path.join(
            os.path.dirname(__file__), './font/qc.ttf'), 24)
        text_surface_1 = chinese_font.render(
            "当前缺肉程度:", False, (0xa5, 0x6f, 0xff))
        text_surface_2 = english_font.render(
            str(self.no_meat)+"/"+str(self.no_meat_limit), False, (0xa5, 0x6f, 0xff))
        surface.blit(text_surface_1, (0, 0))
        surface.blit(text_surface_2, (6*24, 0))


class Animal:
    '''
    动物类
    '''
    update_interval = 500

    def __init__(self, row: int, col: int, map_: Map):
        self.row = row  # 行坐标
        self.col = col  # 列坐标
        self.direction = random.choice(['up', 'down', 'left', 'right'])  # 前进方向
        self.block_type = random.choice(config.ANIMAL_TYPE)  # 动物种类
        self.map_ = map_  # 游戏地图
        self.last_update_time = 0  # 上次更新时间
        self.died = False  # 是否死亡

    def look(self, direction: str):
        '''
        判断指定方向是否能走
        args:
            direction:行进方向(up/down/left/right)
        '''
        if direction == 'stop':
            return False
        dx, dy, _ = config.direction[direction]
        new_row = self.row+dy
        new_col = self.col+dx
        return not self.map_.check_is_out(new_row, new_col) and self.map_.map_[new_row][new_col] not in config.OBSTACLE_TYPE

    def get_direction(self):
        '''
        获取新的行进方向
        '''
        shuffle_direction = list(config.direction)
        random.shuffle(shuffle_direction)
        for direction in shuffle_direction:
            if self.look(direction):
                return direction
        return "stop"

    def go(self):
        '''
        向前走一步
        '''
        if self.look(self.direction):
            dx, dy, _ = config.direction[self.direction]
            self.row += dy
            self.col += dx

    def update(self):
        '''
        动物逻辑更新
        '''

        # 控制执行间隔
        if get_time()-self.last_update_time < self.update_interval:
            return
        self.last_update_time = get_time()

        # 如果被包围,死亡
        if self.is_surrounded():
            self.die()
            return

        # 是否转弯的充分条件
        does_turn = random.randint(1, 100) < 10
        if not self.look(self.direction) or does_turn:  # 如果转弯或前方有障碍
            self.direction = self.get_direction()  # 获取一个新的方向

        # 移动
        self.map_.map_[self.row][self.col] = config.BLOCK_TYPE['null']
        self.go()
        self.map_.map_[self.row][self.col] = self.block_type

    def die(self):
        '''
        死亡
        '''
        self.map_.map_[self.row][self.col] = config.BLOCK_TYPE['meat']  # 变成肉
        self.died = True

    def is_surrounded(self):
        '''
        使用广度优先判断是否被包围
        return:
            是否被包围
        ps:
        其实此函数可以继续优化,但由于性能已经足够,遵循优化原则,就算了.优化方案:
        从地图边界向中央搜索,不断标记节点是否可到达,最终取四个边的集合的并集,可以达到O(地图格子数)的时间复杂度.
        '''

        # 搜索队列
        queue = [(self.row, self.col)]
        # 某个节点是否被访问过(防止绕圈)
        visited = []
        for i in self.map_.map_:
            line = []
            for j in i:
                line.append(False)
            visited.append(line)

        # 广度优先搜索
        while queue:
            pos = queue[0]
            del queue[0]

            # 如果已经到达边界,则没有被包围
            if self.map_.check_is_out(*pos):
                return False

            # 添加新节点
            for direction in config.direction:
                if direction == 'stop':
                    continue
                dx, dy, _ = config.direction[direction]
                new_pos = pos[0]+dy, pos[1]+dx
                # 新节点必须在地图内,否则到达边界
                if self.map_.check_is_out(*new_pos):
                    return False
                # 没有被访问过且
                if not visited[new_pos[0]][new_pos[1]] and self.map_.map_[new_pos[0]][new_pos[1]] not in config.OBSTACLE_TYPE:
                    queue.append(new_pos)
                    visited[new_pos[0]][new_pos[1]] = True
        return True


class Button:
    def __init__(self, img, x, y, w, h):
        self.img = img  # 按钮图像
        # 按钮矩形包围框
        self.x = x
        self.y = y
        self.w = w
        self.h = h

    def is_click(self, x, y):
        '''
        是否被点击
        args:
            x:鼠标横坐标
            y:鼠标纵坐标
        return:
            是否被点击
        '''

        # 如果在矩形包围框外,直接返回False
        if not (x > self.x and x < self.x+self.w and y > self.y and y < self.y+self.h):
            return False

        # 获得鼠标的本地坐标(相对于自身)
        local_x = x-self.x
        local_y = y-self.y
        # 进行像素级判断
        return self.img.get_at((local_x, local_y))[3] != 0

    def draw(self, screen):
        '''
        绘制自身
        args:
            screen:要绘制的对象
        '''
        screen.blit(self.img, (self.x, self.y))


class Game:
    '''
    游戏类,负责游戏流程控制
    '''
    @classmethod
    def init(cls):
        '''
        游戏初始化,在游戏开始时执行一次
        '''
        pygame.init()
        cls.width = 800
        cls.height = 600
        cls.game_status = "start"
        cls.screen = pygame.display.set_mode((cls.width, cls.height))
        pygame.display.set_caption('贪吃蛇生存')
        config.init_image()
        cls.game_over_img = None
        cls.button = Button(config.HELP_BUTTON, *config.help_button_pos)
        Audio.init()
        Audio.play_music('start')

    @classmethod
    def add_animal(cls, animal: Animal):
        '''
        添加动物,将其添加到要更新的动物的数组中
        args:
            animal:要添加的动物
        '''
        cls.animals.append(animal)

    @classmethod
    def gameOver(cls):
        '''
        结束游戏
        '''

        # 播放失败音效
        pygame.mixer.music.pause()
        Audio.play_sound("die")

        cls.game_status = "gameover"

    @classmethod
    def startGame(cls):
        '''
        开始游戏
        '''
        cls.game_status = "playing"
        cls.map_ = Map(config.MAP_ROWS, config.MAP_COLS,
                       config.BLOCK_SIZE, config.BLOCK_SURFACES)
        cls.snake = Snake((
            (0, 2), (0, 1), (0, 0)
        ), cls.map_)
        cls.animals = []
        Audio.play_music("bg")

    @classmethod
    def update(cls):
        '''
        游戏出于playing状态下的更新
        '''

        # 注意更新顺序
        cls.map_.create_food()
        cls.map_.create_animal()
        cls.snake.update()
        # 更新所有动物
        for index, animal in enumerate(cls.animals):
            animal.update()
            if animal.died:
                del cls.animals[index]

        #地图滚动
        head=cls.snake.blocks[0]
        cls.map_.update(head.col*cls.map_.block_size,head.row*cls.map_.block_size)
        
        # 更新完后绘制
        cls.map_.draw(cls.screen)
        cls.snake.draw_text(cls.screen)

    @classmethod
    def show_help(cls):
        '''
        显示帮助文件
        '''
        webbrowser.open(
            'https://gitee.com/fireinice/snake/blob/master/README.md')

    @classmethod
    def start(cls):
        '''开始游戏'''
        cls.init()
        while True:
            pygame.time.wait(1)

            # 游戏中状态
            if cls.game_status == 'playing':
                for e in pygame.event.get():
                    # 退出事件
                    if e.type == pygame.QUIT:
                        pygame.quit()
                        exit()
                        break

                    # 贪吃蛇转向(加速采用键盘轮询)
                    if e.type == pygame.KEYDOWN:
                        if e.key == 273:  # up
                            cls.snake.turn('up')
                        elif e.key == 275:  # right
                            cls.snake.turn('right')
                        elif e.key == 274:  # down
                            cls.snake.turn('down')
                        elif e.key == 276:  # left
                            cls.snake.turn('left')

                # 填充背景色
                cls.screen.fill(config.BACKGROUND_COLOR)
                # 更新游戏
                cls.update()

                # 在游戏结束后保存结束时的图像作为背景
                if cls.game_status == 'gameover':
                    cls.game_over_img = cls.screen.copy()\

            # 游戏结束状态
            elif cls.game_status == 'gameover':
                for e in pygame.event.get():
                    # 退出事件
                    if e.type == pygame.QUIT:
                        pygame.quit()
                        exit()
                        break
                    # 重新开始事件
                    if e.type == pygame.KEYDOWN:
                        cls.game_status = "start"
                        Audio.play_music('start')

                # 绘制游戏结束界面
                cls.screen.blit(cls.game_over_img, (0, 0))
                cls.screen.blit(config.GAMEOVER_IMG, (0, 0))

            # 游戏开始界面状态
            elif cls.game_status == 'start':
                for e in pygame.event.get():
                    # 退出事件
                    if e.type == pygame.QUIT:
                        pygame.quit()
                        exit()
                        break
                    # 开始游戏事件
                    if e.type == pygame.KEYDOWN:
                        cls.startGame()

                    # 如果帮助按钮被点击,显示帮助
                    if e.type == pygame.MOUSEBUTTONUP:
                        x, y = pygame.mouse.get_pos()
                        if cls.button.is_click(x, y):
                            cls.show_help()
                # 绘制图像
                cls.screen.blit(config.GAMESTART_IMG, (0, 0))
                cls.button.draw(cls.screen)

            pygame.display.update()


def main():
    Game.start()


main()
